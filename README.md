# Sequelize Demo

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> A Sequelize Demo for NodeJS and Express

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```bash
$ npm install
```

## Usage

### Development Database

If you would like to run a development database then you should edit the environment variables in `docker-compose.yml` and run the following command:

```bash
$ docker-compose up -d
```

To remove the running container, run the following:

```bash
$ docker-compose down
```

#### Caveat

With the current configuration of the `docker-compose.yml` file, the database contents will not be persisted between restarts. This should not be used in a production deployment.

### Configuration

You should copy the `config/config.example.json` file to `config/config.json` and edit the values.

If connecting to a fresh database then you should run migrations with the following command:

```bash
$ npx sequelize-cli db:migrate
```

### Development

```bash
$ npm run dev
```

### Test User Endpoints

#### GET Users

```bash
$ curl -vvv -X GET http://localhost:3000/api/v1/user
```

#### GET User :id

```bash
$ curl -vvv -X GET http://localhost:3000/api/v1/user/<id>
```

#### POST User

```bash
$ curl -vvv -X POST -H "Content-Type: application/json" -d '{"firstName":"Greg","lastName":"Linklater","email":"hello@world.com"}' http://localhost:3000/api/v1/user
```

#### PUT User

```bash
$ curl -vvv -X PUT -H "Content-Type: application/json" -d '{"firstName":"Greg","lastName":"Linklater","email":"test@test.com"}' http://localhost:3000/api/v1/user/<id>
```

#### PATCH User

```bash
$ curl -vvv -X PATCH -H "Content-Type: application/json" -d '{"email":"yet@another.change"}' http://localhost:3000/api/v1/user/<id>
```

#### DELETE User

```bash
$ curl -vvv -X DELETE http://localhost:3000/api/v1/user/<id>
```

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Noroff Accelerate AS
