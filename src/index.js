
/**
 * Dependencies
 * @ignore
 */
const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')

/**
 * Module Dependencies
 * @ignore
 */
const users = require('./user')

/**
 * App
 * @ignore
 */
const app = express()

// Logger
app.use(morgan('tiny'))

// Body parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

/**
 * Endpoints
 * @ignore
 */

// Mount users
users(app, '/api/v1/user')

// default error handler
app.use((err, req, res, next) => {
  if (err) {
    console.error(err)
    return res.status(500).end()
  }

  next()
})

/**
 * Listen
 * @ignore
 */
const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Server listening on port ${port}`))
