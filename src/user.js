'use strict'

/**
 * Dependencies
 * @ignore
 */
const _ = require('lodash')

/**
 * Module Dependencies
 * @ignore
 */
const express = require('express')
const db = require('../models')

/**
 * Endpoints
 * @ignore
 */
function users (app, mount_path) {
  const router = express.Router()

  // GET /user => Array.User
  router.get('/', (req, res) => {
    db.User.findAll()
      .then(users => {
        res.status(200).json(users.map(item => item.dataValues))
      })
  })

  // POST /user => User (and Location header)
  router.post('/', (req, res) => {
    const { body = {} } = req
    const {
      firstName,
      lastName,
      email,
    } = body

    // Check if properties exist
    if (!firstName) {
      return res.status(400).json({ error: 'firstName required' })
    }

    if (!lastName) {
      return res.status(400).json({ error: 'lastName required' })
    }

    if (!email) {
      return res.status(400).json({ error: 'email required' })
    }

    // Run database insert
    db.User.create({ firstName, lastName, email })
      .then(result => {
        console.log('RESULT', result)
        res.setHeader('Location', `${mount_path}/${result.id}`)
        res.status(201).json(result.dataValues)
      })
      .catch(err => console.error(err))
  })

  // GET /user/:id => User
  router.get('/:id', (req, res) => {
    const { id } = req.params

    db.User.findByPk(id)
      .then(user => {
        if (user) {
          return res.status(200).json(user.dataValues)
        }

        return res.status(404).end()
      })
      .catch(err => {
        console.error(err)
        res.status(400).end()
      })
  })

  // PUT /user/[:id] => User
  const upsert = (req, res) => {
    const { body = {} } = req

    if (!body.id && req.params.id) {
      body.id = req.params.id
    }

    const {
      id,
      firstName,
      lastName,
      email,
    } = body

    // Check if properties exist
    if (!id) {
      return res.status(400).json({ error: 'id required' })
    }

    if (!firstName) {
      return res.status(400).json({ error: 'firstName required' })
    }

    if (!lastName) {
      return res.status(400).json({ error: 'lastName required' })
    }

    if (!email) {
      return res.status(400).json({ error: 'email required' })
    }

    db.User.upsert(body, { returning: true })
      .then(user => res.status(200).json(user.dataValues))
  }

  router.put('/', upsert)
  router.put('/:id', upsert)

  // PATCH /user/[:id]
  router.patch('/:id', (req, res) => {
    const { id } = req.params
    const { body = {} } = req

    // Pulls the properties defined on `body` with the corresponding field names if they exist.
    const patch = _.pick(body, [
      'firstName',
      'lastName',
      'email',
    ])

    db.User.findByPk(id)
      .then(user => {
        if (user) {
          return user.set(patch).save()
        }

        return res.status(404).end()
      })
      .then(user => res.status(200).json(user.dataValues))
  })

  // DELETE /user/:id => (void)
  router.delete('/:id', (req, res) => {
    const { id } = req.params

    db.User.findByPk(id)
      .then(user => {
        return user.destroy()
      })
      .then(() => {
        res.status(204).end()
      })
  })

  app.use(mount_path, router)
}

/**
 * Exports
 * @ignore
 */
module.exports = users
