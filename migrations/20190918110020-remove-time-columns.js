'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Users', 'created')
    await queryInterface.removeColumn('Users', 'modified')
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Users', 'created', Sequelize.DATE)
    await queryInterface.addColumn('Users', 'modified', Sequelize.DATE)
  }
};
